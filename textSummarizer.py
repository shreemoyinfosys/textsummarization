#!/usr/bin/env python
# coding: utf-8

# In[1]:


from summarizer import Summarizer
import spacy
from spacy.lang.en.stop_words import STOP_WORDS
from string import punctuation
from heapq import nlargest
import logging
import sys
logging.basicConfig(level=logging.DEBUG)


# In[1]:


class TextSummarizer:
    def bertSummerizer(self,bodyOfEmail):
        logging.debug("Inside bertSummerizer")
        try:
            model = Summarizer()
            result = model(bodyOfEmail, min_length=10)
            summary = "".join(result)
        except:
            logging.debug("Exception {} occured, could not summarize text".format(sys.exc_info()[0]))
            return bodyOfEmail
        return summary

    def spaceySummerizer(self,body):
        loggig.debug("Inside spaceySummerizer")
        try:
            text = body
            stopwords = list(STOP_WORDS)
            nlp = spacy.load('en_core_web_sm')
            doc = nlp(text)
            tokens = [token.text for token in doc]

            logging.debug("Tokens : {}".format(tokens))
    #         punctuation = punctuation + '\n'

            word_frequencies = {}
            for word in doc:
                if word.text.lower() not in stopwords:
                    if word.text.lower() not in punctuation:
                        if word.text not in word_frequencies.keys():
                            word_frequencies[word.text] = 1
                        else:
                            word_frequencies[word.text] += 1
            logging.debug("Word frequencies : {}".format(word_frequencies))

            max_frequency = max(word_frequencies.values())

            for word in word_frequencies.keys():
                word_frequencies[word] = word_frequencies[word]/max_frequency
            logging.debug("Word frequencies : {}".format(word_frequencies))    


            sentence_tokens = [sent for sent in doc.sents]
            logging.debug("Sentence Tokens : {}".format(sentence_tokens))

            sentence_scores = {}
            for sent in sentence_tokens:
                for word in sent:
                    if word.text.lower() in word_frequencies.keys():
                        if sent not in sentence_scores.keys():
                            sentence_scores[sent] = word_frequencies[word.text.lower()]
                        else:
                            sentence_scores[sent] += word_frequencies[word.text.lower()]

            logging.debug("Sentence Scores : {}".format(sentence_scores))

            select_length = int(len(sentence_tokens)*0.3)

            summary = nlargest(select_length, sentence_scores, key = sentence_scores.get)
            final_summary = [word.text for word in summary]
            summary = ' '.join(final_summary)
        except:
            logging.debug("Exception {} occured, could not summarize text".format(sys.exc_info()[0]))
            return body
            
#         print(summary)
        return summary

