#!/usr/bin/env python
# coding: utf-8

# In[6]:


import imaplib
import email
from email.header import decode_header
import webbrowser
import os
import re
import yaml
import logging
logging.basicConfig(level=logging.DEBUG)
import sys
import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 


# In[1]:


class FetchEmails:
    def __init__(self,filename):
        with open(filename,"r") as file:
            self.documents = yaml.full_load(file)

            for item, doc in self.documents.items():
                #print(item, ":", doc)
                logging.debug("Item: {}   ".format(item))
            
            
            self.hostName = self.documents.get('host')
            self.loginId = self.documents.get('loginId')
            self.loginPwd = self.documents.get('loginPwd')
            self.senderId = self.documents.get('senderId')
        self.TAG_RE = re.compile(r'<[^>]+>')
        self.mail = imaplib.IMAP4_SSL(self.hostName)
        self.mail.login(self.loginId, self.loginPwd)
        self.mail.select("inbox")
        
        
    
    def remove_tags(self,text):
        return self.TAG_RE.sub('', text)

    def get_searchdata(self):
        try:
            _, search_data = self.mail.search(None, 'SINCE "07-Dec-2020" FROM "{}"'.format(self.senderId))
        except:
            logging.debug("Exception {} occured, could not find search data".format(sys.exc_info()[0]))
        return search_data
    
    def get_inbox(self,num):
        logging.debug("Inside get_inbox to fetch mail")
        my_message = []
        email_data = {}
        filename = None
        body =None
        try:
            _, data = self.mail.fetch(num, '(RFC822)')
            for response in data:
                if isinstance(response, tuple):
                    # parse a bytes email into a message object
                    msg = email.message_from_bytes(response[1])
                    # decode the email subject
                    subject = decode_header(msg["Subject"])[0][0]
                    if isinstance(subject, bytes):
                        # if it's a bytes, decode to str
                        subject = subject.decode()
                    # decode email sender
                    From, encoding = decode_header(msg.get("From"))[0]
                    if isinstance(From, bytes):
                        From = From.decode(encoding)
                        #print("Subject:", subject)
                        #print("From:", From)
                    # if the email message is multipart
                    if msg.is_multipart():
                        # iterate over email parts
                        logging.debug("Message is multipart")
                        for part in msg.walk():
                            # extract content type of email
                            content_type = part.get_content_type()
                            content_disposition = str(part.get("Content-Disposition"))
                            try:
                                # get the email body
                                body = part.get_payload(decode=True).decode()
                            except:
                                pass
    #                         if content_type == "text/plain" and "attachment" not in content_disposition:
    #                             # print text/plain emails and skip attachments
    # #                             print(body)
                            try:
                                if "attachment" in content_disposition:
                                    # download attachment
                                    filename = part.get_filename()
                                    if filename:
        #                                 if not os.path.isdir(subject):
        #                                     # make a folder for this email (named after the subject)
        #                                     os.mkdir(subject)
                                        filepath = os.getcwd() + "/email_attachments/"
                                        filepath = os.path.join(filepath, filename)
                                        # download attachment and save it
                                        open(filepath , "wb").write(part.get_payload(decode=True))
                            except:
                                logging.debug("Exception {} occured, in attachment".format(sys.exc_info()[0]))
                                return body,subject,filename
                                
                    else:
                        logging.debug("Message is not multipart")
                        # extract content type of email
                        content_type = msg.get_content_type()
                        # get the email body
                        body = msg.get_payload(decode=True).decode()
    #                     if content_type == "text/plain":
                            # print only text email parts
    #                         print(body)
    #                 if content_type == "text/html":
    #                     # if it's HTML, create a new HTML file and open it in browser
    #                     if not os.path.isdir(subject):
    #                         # make a folder for this email (named after the subject)
    #                         os.mkdir(subject)
    #                     filename = f"{subject[:50]}.html"
    #                     filepath = os.path.join(subject, filename)
    #                     # write the file
    #                     open(filepath, "w").write(body)
    #                     # open in the default browser
    #                     webbrowser.open(filepath)
                    print("="*100)

                body = self.remove_tags(body)
                print("="*100)
        except:
            logging.debug("Exception {} occured, while fetching email".format(sys.exc_info()[0]))
            return None,None,None
        return body,subject,filename

    def closeEmail(self):
        logging.debug("Inside closeEmail")
        self.mail.close()
        self.mail.logout()
        
    def sendSummarizedEmail(self,summarizedBody,subjectOfEmail,attachmentFile):
        
        logging.debug("Inside sendSummarizedEmail")
        
        self.toaddr = str(self.loginId)
        msg = MIMEMultipart()    
        msg['From'] =  str(self.loginId)
        msg['To'] = self.toaddr 
        msg['Subject'] = subjectOfEmail
        body = summarizedBody
        msg.attach(MIMEText(body, 'plain')) 
        #print(self.fileNameNewWorkbook)
        if(attachmentFile !=None):
            logging.debug("File Attachment Name :  {}".format(attachmentFile))
            
            attachment = open(os.getcwd()+"/email_attachments/"+attachmentFile, "rb") 

            p = MIMEBase('application', 'octet-stream') 
            p.set_payload((attachment).read()) 
            encoders.encode_base64(p) 
            p.add_header('Content-Disposition', 'attachment', filename=attachmentFile) 
            msg.attach(p)
        text = msg.as_string()
        return text
            
    def sendEmail(self,summarizedBody,subjectOfEmail,filename):
        logging.debug("Inside sendEmail")
        try:
            msgText =self.sendSummarizedEmail(summarizedBody,subjectOfEmail,filename)
            s = smtplib.SMTP('smtp.gmail.com', 587) 
            s.starttls() 
            s.login(self.loginId,self.loginPwd)
        except :
             logging.debug("Exception {} occured, connecting to email client".format(sys.exc_info()[0]))
        try:
            s.sendmail(str(self.loginId), str(self.loginId), msgText) 
            print("mail sent successfully")
        except smtplib.SMTPException as e:
            print("Unable to send email -r" ,e)
            s.quit()


# In[ ]:




