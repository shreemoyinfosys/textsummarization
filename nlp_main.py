#!/usr/bin/env python
# coding: utf-8

# In[1]:


from FetchEmailText import FetchEmails
from textSummarizer import TextSummarizer
import os,sys


# In[2]:


def main():
    
    curDir = os.getcwd() 
    fileName = curDir + "/support_files/configurable_details.yml"
    fetchEmailBody = FetchEmails(fileName)
    textSummarizer = TextSummarizer()
    search_data = fetchEmailBody.get_searchdata()
    print("split ", search_data[0].split())
    
    for num in search_data[0].split():
        bodyOfEmail,subjectOfEmail,filename = fetchEmailBody.get_inbox(num)
        print(bodyOfEmail)
        if (bodyOfEmail!= None):
            summarizedBodyBert = textSummarizer.bertSummerizer(bodyOfEmail)
#         summarizedBodySpacy =textSummarizer.spaceySummerizer(bodyOfEmail)
        print(summarizedBodyBert)
        fetchEmailBody.sendEmail(summarizedBodyBert,subjectOfEmail,filename)
#         print(summarizedBodySpacy)
    
    fetchEmailBody.closeEmail()
# result = model(body, ratio=0.2)  # Specified with ratio
# result = model(body, num_sentences=3)  # Will return 3 sentences 


if __name__ == "__main__":
    print("="*30 + "Execution Started" + "="*30)
    main()
    print("="*30 + "Execution Ended" + "="*30)

